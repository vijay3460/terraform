provider "aws" {}

resource "aws_vpc" "my-vpc" {
    cidr_block = "10.0.0.0/16"

    tags = {
        Name = "myvpc"
    }
}


resource "aws_subnet" "my-subnet" {
    vpc_id = aws_vpc.my-vpc.id
    cidr_block = "10.0.0.0/24"
    availability_zone = "ap-south-1a"
    tags = {
        Name = "my-subnet"
    }
}

resource "aws_internet_gateway" "my-igw" {
    vpc_id = aws_vpc.my-vpc.id

    tags = {
        Name = "my-igw"
    }
}

resource "aws_default_route_table" "my-rt" {
    default_route_table_id = aws_vpc.my-vpc.default_route_table_id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.my-igw.id
    }
}
